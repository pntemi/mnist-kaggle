from framework import run

# 1.) Setup your config
# args variable contains the configuration for the submission or the experiment we want to run.
# This is should be the ONLY thing we have to change for each run.
args = {
    'submission': True,
    'train_fraction': 0.8,
    'random_state': 2,
    'expt': 'expt_2',
    'cross_val_score': True
}

if __name__ == "__main__":
    run(args)

