from datetime import datetime
import utils
import os

import pandas as pd
import numpy as np
from sklearn.externals import joblib
from sklearn.cross_validation import cross_val_score, cross_val_predict, train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix

from models import experiment_dict

def run(args):

    # import required utilities
    if not args['submission']:
        import matplotlib.pyplot as plt
        import seaborn as sb

    # 2.)Load data for training model
    #    Full dataset for training in case of submission
    #    Split dataset into train and test for cross-validation
    X_train_full, y_train_full, X_test = load_and_prepare_data()

    if args['submission']:
        # making a submission; train on all given data
        print('fitting models to entire training set')
        X_train, y_train = X_train_full, y_train_full
    else:
        # running an experiment - cross validate with train/test split
        train_fraction = 0.8
        print('fitting models to cv train/test split with train% = {}'.format(train_fraction))
        X_train, X_test, y_train, y_test = train_test_split(X_train_full,y_train_full, test_size=args['train_fraction'], random_state=args['random_state'])


    # 3.)Get pipeline
    pipeline_detail = experiment_dict[args['expt']]
    pipeline = pipeline_detail['pl']

    #   3.1)Fit model to training data
    print('fitting model to array sizes (xtrain, ytrain)={}'.format(
                                            [i.shape for i in [X_train, y_train]]))
    print('fitting experiment pipeline with signature={}'.format(pipeline))
    pipeline.fit(X_train, y_train)

    # 4)For non-submission experiments, we get best parameters from grid search
    if args['submission']:
        fname_spec = '_submission_'
    else:
        # gridsearch? log all results + call out the winner 
        if hasattr(pipeline, 'best_params_'):
            print('best gridsearch score={}'.format(pipeline.best_score_))
            print('best set of pipeline params={}'.format(pipeline.best_params_))
            print('now displaying all pipeline param scores...')
            for params, mean_score, scores in pipeline.grid_scores_:
                print("{:0.3f} (+/-{:0.03f}) for {}".format(mean_score, scores.std() * 2, params))
        fname_spec = '_expt_'

    model_name = utils.short_name(pipeline) + \
                 fname_spec + \
                 datetime.utcnow().strftime('%Y-%m-%d_%H%M%S')

    try:
        joblib.dump(pipeline, os.path.join('saved_models', model_name) + '.pkl', compress=3)
    except OverflowError, e:
        print('joblib write failed with error={}'.format(e))
        print('proceeding with predictions without writing model to disk')

    # 5) Prepare submission
    if args['submission']:
        # make predictions for a leaderboard submission
        print('writing predictions to formatted submission file')
        predictions = pipeline.predict(X_test)
        if hasattr(pipeline, 'best_params_'):
            print('predicting test values with best-choice gridsearch params')
        utils.create_submission(predictions,
                                pipeline_detail['name'])
    else:
        # otherwise, run a cross-validation for test accuracy
        # 4.) This belogs to step 4 where print out accuracy and confusion matrix
        cv = 3
        print('cross validating model predictions with cv={}'.format(cv))
        predictions = cross_val_predict(pipeline, X_test, y_test, cv=cv)
        print('obtained accuracy = {:.2f}% with cv={}, pipeline={} '.format(
            accuracy_score(y_test, predictions) * 100,
            cv,
            pipeline))
        if args['cross_val_score']:
            # this gives a better idea of uncertainty, but it adds 'cv' more
            #   fits to the pipeline (expensive!)
            print('cross validating model accuracy with cv={}'.format(cv))
            scores = cross_val_score(pipeline, X_test, y_test, cv=cv)
            print('obtained accuracy={:0.2f}% +/- {:0.2f} with cv={}, \
                                        pipeline={} '.format(scores.mean() * 100,
                                                             scores.std() * 100 * 2,
                                                             cv,
                                                             pipeline))

        # if running an experiment, plot confusion matrix for review
        print('calculating confusion matrix')
        try:
            sb.heatmap(confusion_matrix(y_test, predictions))
        except RuntimeError, e:
            print('plotting error. matplotlib backend may need to be changed (see readme). error={}'.format(e))
            print('plot may still have been saved, and model has already been saved to disk.')
        try:
            plt.title(model_name + ' [expt] ({:.2f}%)'.format(scores.mean() * 100))
        except NameError:
            print('didnt find "scores" from cross_val_score, calculating accuracy by accuracy_score()')
            plt.title(model_name + ' [expt] ({:.2f}%)'.format(accuracy_score(y_test, predictions) * 100))
        plt.xlabel("True")
        plt.ylabel("Pred")
        print('saving confusion matrix')
        plt.savefig(os.path.join('saved_models', model_name) + '.pdf',
                    format='pdf',
                    bbox_inches='tight')

    print('completed with pipeline {}'.format(pipeline_detail['name']))


# This is where 2.) calls to
#if .npy exists, read from .npy else read from csv
def load_and_prepare_data():
    try:
        if(os.path.isfile("data/train.npy") and os.path.isfile("data/test.npy")):
            X_train = np.load(os.path.join('data', 'train.npy'))
            X_test = np.load(os.path.join('data', 'test.npy'))
        else:
            train = pd.read_csv("data/train.csv")
            test = pd.read_csv("data/test.csv")
            with open('data/{}.npy'.format('train'), 'wb') as f:
                np.save(f, train)
            with open('data/{}.npy'.format('test'), 'wb') as f:
                np.save(f, test)
            X_train = np.array(train)
            X_test = np.array(test)

        X_train_full = np.apply_along_axis(lambda row: row[1:], 1, X_train)
        y_train_full = np.apply_along_axis(lambda row: row[0], 1, X_train)

        print('observed data dimensions: {}, {}. {}'.format(
            X_train_full.shape, y_train_full.shape, X_test.shape))

        return X_train_full, y_train_full, X_test
    except IOError, e:
        print('Error message={}'.format(e))
        raise e

